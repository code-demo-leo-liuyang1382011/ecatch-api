# Overview
eCatch is a project staring on 2019 for VFA (Vic Fisheries Authority), which mainly includes 
- management of fishing charters,  fishing trip, quota and weight calculation, and disposal for abalone, rock lobster and crab etc of each trip.
- User authentication (JWT) and role-based access to resources.
- Email and push notification services for various services.

## Technical stack
1. Node.js + Express
2. Typescript
3. AWS serverless framework (SAM) + AWS S3 + AWS Cloudfront

# Note
- This project is a skeleton of the project, and all related business logic has been removed. So It won't be able to run but for a purpose of code demo. Because of this, all the unit tests and black box testing on the business logics are also removed. 
- Any related CI/CD configuration files are also removed, and also all the environment variables containing the credentials, and keys.
- However, there are some *.spec.ts files for testing utilities.
- Also in the production environment, multi-staged definition is also done in the cloudformation.yaml file.


# Run the following setup scripts
```bash
nvm use
npm i
npm run ts-clear

# Make sure to replace:
npm run config -- --account-id="YOUR_ACCOUNT_ID" \
--bucket-name="YOUR_UNIQUE_BUCKET_NAME" \
--region="YOUR_AWS_REGION" \
--function-name="YOUR_SERVERLESS_EXPRESS_LAMBDA_FUNCTION_NAME" \
--stack-name="YOUR_SERVERLESS_EXPRESS_STACK_NAME" \
--api-name="YourProjectApi"

npm run setup

https://console.aws.amazon.com/cloudformation/home
```

# Locally test, modify API and deploy
```bash
npm run local

npm start

npm run package-deploy
```

https://github.com/lambci/docker-lambda

docker run --rm -v "$PWD":/var/task lambci/lambda:nodejs10.x lambda.js


# build, updload and deploy scripts

Update the prefix in deploy-commons.sh

```bash
export S3_API_KEY_PREFIX='your-projects/api'
```

