/**
 * Created by hxg on 9/10/17.
 * converted to TS by kis on 3/7/18
 */
import { createLog } from '../logs/logging';
const log = createLog(__filename);

import { equals } from '../ramda-functions';
import { ConnectionPool, config } from 'mssql';
import { MAX_CONNECTION_POOL } from './database';

class Connection {
  private static db: any;
  private static dbConfig: config = {
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    server: `${process.env.DATABASE_HOST}\\${process.env.DATABASE_INSTANCE_NAME}`,
    database: process.env.DATABASE_NAME,
    options: {
      encrypt: false,
      useUTC: false,
    },
    pool: {
      max: MAX_CONNECTION_POOL,
    },
  };
  private deferredConnection: ConnectionPool;

  static init () {
    
  }

  static getDb () {
    log.silly('DbConnection.db');

    if(!this.db) {
      this.init();
    }

    return this.db;
  }

  static pgpHelper () {
    return pgp.helpers;
  }
}

export default Connection;
