import { identity, isNil } from '../ramda-functions';

export const MAX_CONNECTION_POOL = 1;

export const transformerOrNone = (transformer: any): any => {
  return isNil(transformer) ? identity : transformer;
};
